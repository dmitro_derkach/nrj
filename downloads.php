<?php

ini_set("max_execution_time", "0");
header('Content-Type: application/json');
if (stripos(PHP_OS, "linux") !== false)
    mb_internal_encoding('utf-8');

stream_context_set_default(array(
    'http' => array(
        'timeout' => 10,
    )
));

if (!is_dir("zip"))
    mkdir("zip");

function build_file_name($filename) {
    $result = "";
    $filename = mb_substr($filename, 0, -4);
    for ($i = 0; $i < mb_strlen($filename); $i++) {
        if ($i > 100)
            break;
        if (preg_match("/^\w*\s*-*&*\(*\)*_*$/u", mb_substr($filename, $i, 1))) {
            $result .= mb_substr($filename, $i, 1);
        }
    }
    $result .= ".mp3";
    if (stripos(PHP_OS, "linux") === false)
        $string = @iconv("UTF-8", "Windows-1251", $result);
    if (isset($string) && $string)
        return $string;
    else
        return $result;
}

function delFolder($dir) {
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? rmdir(delFolder("$dir/$file")) : unlink("$dir/$file");
    }
    return $dir;
}

function Zip($source, $destination, $index, $part) {
    if (stripos(PHP_OS, "linux") === false || true){
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }
        $break = false;
        if (is_dir($source) === true) {
            $index = (int) $index;
            $part = (int) $part;
            if (!$part)
                $part = 1;
            $filesize = 0;
            $zip = new ZipArchive();
            if (!$zip->open("zip/" . (($part > 1) ? pathinfo($destination, PATHINFO_FILENAME) . "(" . ($part - 1) . ").zip" : $destination), ZIPARCHIVE::CREATE)) {
                return false;
            }

            $source = str_replace('\\', '/', realpath($source));

            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
            $j = 0;
            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                    continue;
                $j++;
                if ($j <= $index)
                    continue;
                $file = realpath($file);
                $file = str_replace('\\', '/', $file);
                $filesize+=filesize($file);
                if ($filesize > 1.5 * 1000 * 1000 * 1000) {
                    $break = true;
                    break;
                }
                $index = $j;
                if (stripos(PHP_OS, "linux") === false)
                    $filename = @iconv("Windows-1251", "UTF-8", $file);
                else
                    $filename = $file;
                if (empty($filename))
                    $filename = $file;
                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $filename . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFile($file, str_replace($source . '/', '', $filename));
                }
            }
            $zip->close();
        } else if (is_file($source) === true) {
            $zip->addFile(basename($source), basename($source));
            $zip->close();
        }
        if ($break)
            return json_encode(array('index' => $index, 'part' => $part, "success" => false, "filename" => ($part > 1) ? pathinfo($destination, PATHINFO_FILENAME) . "(" . ($part - 1) . ").zip" : $destination));
        else {
            if (is_dir("downloads"))
                delFolder("downloads");
            return json_encode(array("success" => true, "filename" => ($part > 1) ? pathinfo($destination, PATHINFO_FILENAME) . "(" . ($part - 1) . ").zip" : $destination));
        }
    }
    else{
        $break = false;
        $index = (int) $index;
        $part = (int) $part;
        if (!$part)
            $part = 1;
        $filesize = 0;
        $source = str_replace('\\', '/', realpath($source));
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
        $j = 0;
        $file_names = '';
        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                continue;
            $j++;
            if ($j <= $index)
                continue;
            $file = realpath($file);
            $file = str_replace('\\', '/', $file);
            $filesize+=filesize($file);
            if ($filesize > 1.5 * 1000 * 1000 * 1000) {
                $break = true;
                break;
            }
            $index = $j;
            $file_names .= '"' . str_replace($source . '/', '', $file) . '" ';
        }
        $zip_name = (($part > 1) ? pathinfo($destination, PATHINFO_FILENAME) . "(" . ($part - 1) . ").zip" : $destination);
        shell_exec("cd $source && zip -r \"../zip/$zip_name\" $file_names");
        if ($break)
            return json_encode(array('index' => $index, 'part' => $part, "success" => false, "filename" => ($part > 1) ? pathinfo($destination, PATHINFO_FILENAME) . "(" . ($part - 1) . ").zip" : $destination));
        else {
            if (is_dir("downloads"))
                delFolder("downloads");
            return json_encode(array("success" => true, "filename" => ($part > 1) ? pathinfo($destination, PATHINFO_FILENAME) . "(" . ($part - 1) . ").zip" : $destination));
        }
    }
}

function download($mode) {

    if (!is_dir("downloads"))
        mkdir("downloads");

    switch ($mode) {
        case 'flush':
            if (is_dir("downloads"))
                delFolder("downloads");
            if (is_dir("zip"))
                delFolder("zip");
            return json_encode(true);
        case 'addFile':
            return json_encode(move_uploaded_file($_FILES['data']['tmp_name'], "downloads/" . build_file_name($_REQUEST['filename'])));
            break;
        case 'addLink':
            $data = @file_get_contents($_REQUEST['link']);
            if ($data !== false)
                return json_encode(file_put_contents("downloads/" . build_file_name($_REQUEST['filename']), $data));
            else
                return json_encode(false);
            break;
        case 'create':
            if (!is_dir("zip"))
                mkdir("zip");

            if (isset($_REQUEST['index']))
                $index = $_REQUEST['index'];
            else
                $index = null;
            if (isset($_REQUEST['part']))
                $part = $_REQUEST['part'];
            else
                $part = null;
            return Zip("downloads", "download.zip", $index, $part);
            break;
        case 'download':
            $file = "zip/" . $_REQUEST['filename'];
            if (file_exists($file)) {
                if (ob_get_level()) {
                    ob_end_clean();
                }
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                unlink($file);
                exit;
            } else
                http_response_code(500);
            break;
        default:
            return json_encode(false);
            break;
    }
}

if ($_REQUEST['op']) {
    echo download($_REQUEST['op']);
}
?>