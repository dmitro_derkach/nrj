<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="favicon.png" rel="shortcut icon"/>
    <script type="text/javascript" src="recorder.js"></script>
    <script type="text/javascript" src="jsmediatags.js"></script>
    <script type="text/javascript" src="jschardet.min.js"></script>
    <script type="text/javascript" src="stringview.js"></script>
    <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jquery.contextMenu.min.js"></script>
    <script type="text/javascript" src="cache.js"></script>
    <script type="text/javascript" src="bootstrap.min.js"></script>
    <script type="text/javascript" src="moment-with-locales.min.js"></script>
    <script type="text/javascript" src="bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="jquery.contextMenu.min.css"/>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <link rel="stylesheet" href="bootstrap-datetimepicker.min.css"/>
    <style type="text/css">
        html, body {
            height: 100%;
        }

        body {
            color: #FEFEFE;
            background-color: #272822;
            margin: 0;
            background: url("site-bg.jpg") no-repeat scroll 0px 0px;
            background-attachment: fixed;
            background-size: cover;
        }

        #track {
            margin-bottom: 0;
            margin-top: 10px;
            overflow: auto;
            min-height: 250px;
        }

        .eq {
            writing-mode: bt-lr; /* IE */
            -webkit-appearance: slider-vertical; /* WebKit */
            width: 20px !important;
            display: inline-block !important;
        }

        .wrap_eq {
            display: none;
            overflow: auto;
        }

        #canvas:-webkit-full-screen {
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            height: 100%;
            background-color: black;
        }

        .control_img {
            width: 25px;
            display: inline-block;
            vertical-align: middle;
            margin-left: -25px;
            padding: 5px 0px;
        }

        .wrap_list {
            display: inline-block;
            margin-left: 10px;
            vertical-align: middle;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .playlist {
            list-style-type: none;
        }

        #track > div {
            padding-left: 25px;
            margin-left: -25px;
            cursor: pointer;
            position: relative;
        }

        #track > div li {
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Chrome/Safari/Opera */
            -khtml-user-select: none; /* Konqueror */
            -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none;
            word-break: break-all;
        }

        #track > div:hover {
            background-color: rgba(255, 255, 255, 0.3) !important;
        }

        #track > div[data-selected="true"] {
            background-color: rgba(255, 255, 255, 0.3) !important;
        }

        #track > div[data-cached="true"] {
            background-color: rgba(0, 255, 0, 0.1);
        }

        #track > div[data-current="true"] {
            background-color: rgba(255, 255, 255, 0.2) !important;
        }

        #track > div[data-current="true"]:hover {
            background-color: rgba(255, 255, 255, 0.3) !important;
        }

        #track > div[data-cached="true"][data-current="true"] {
            background-color: rgba(127, 255, 127, 0.2) !important;
        }

        #track > div[data-cached="true"]:hover {
            background-color: rgba(127, 255, 127, 0.3) !important;
        }

        #track > div[data-cached="true"][data-selected="true"] {
            background-color: rgba(127, 255, 127, 0.3) !important;
        }

        .download {
            background-color: blue;
            position: absolute;
            bottom: 5px;
            margin-left: 10px;
            width: 0;
            height: 1px;
        }

        #custom_add {
            display: none;
        }

        button, input, optgroup, select, textarea {
            color: initial !important;
        }

        @media screen and (max-width: 850px) {
            table:not(.eq_table) td {
                display: block;
            }
        }

        @media screen and (max-width: 520px) {
            #track {
                left: 0 !important;
            }
        }
    </style>
</head>
<body style="padding: 5px;">
<div style="text-align:center;display: table;height:100%;width:100%;table-layout: fixed;">
    <div style="display:table-row;width:100%">
        <div style="display: table-cell;width:100%;">
            <table style="margin:auto;">
                <tr>
                    <td rowspan="2" style="max-width: 200px;margin:auto;">
                        <label>Volume</label>
                        <label id="vol_value">50%</label>
                        <div style="margin-top: 5px">
                            <input id="volume" type="range" min="0" max="100" step="1" value="50"
                                   style="vertical-align:middle;"/>
                        </div>
                    </td>
                    <td style="padding-left:5px;">
                        <select id="radio_list" style="width:100%;">
                            <option value="0">Online</option>
                            <option value="1">Hot 40</option>
                            <option value="2">All hits</option>
                            <option value="3">Party hits</option>
                            <option value="4">Custom *.mp3</option>
                            <option value="5">vk.com</option>
                            <option value="6">Custom radio`s</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:5px;">
                        <button id="manage">Play</button>
                        <div style="position:relative;display:inline-block;color:initial;">
                            <input type="button" id="sleep_timer" value="Sleep"/>
                        </div>
                        <button id="rec">Record</button>
                        <button id="toggle_eq" data-show="0">Show equalizer</button>
                        <form id="reset_file" style="display: none">
                            <input id="files" type="file" multiple accept="audio/*"/>
                        </form>
                        <button id="select_files" style="display: none;">Select *.mp3</button>
                        <div id="loop_block" style="display:none;">
                            <input type="checkbox" name="loop" id="loop"/>
                            <label for="loop">Loop</label>
                        </div>
                        <div id="cache_block" style="display:none;">
                            <input type="checkbox" name="cache" id="cache"/>
                            <label for="cache">Cache</label>
                        </div>
                        <button id="cache_all" style="display: none">Cache all</button>
                        <button id="clear_cache" style="display: none">Clear cache</button>
                        <button id="download_playlist" style="display: none">Download playlist</button>
                        <button id="custom_add">Add radio</button>
                    </td>
                </tr>
            </table>
            <div class="wrap_eq">
                <div style="display: inline-block;">
                    <table class="eq_table">
                        <tr>
                            <td>
                                <input class="eq" id="eq_1" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_2" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_3" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_4" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_5" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_6" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_7" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_8" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_9" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_10" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_11" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_12" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_13" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_14" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_15" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_16" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_17" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                            <td>
                                <input class="eq" id="eq_18" type="range" min="-15" max="15" step="0.1" value="0"
                                       orient="vertical"/>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <input id="reset_eq" type="button" value="Reset"/>
                        <input id="manage_eq" type="button" value="Off"/>
                    </div>
                </div>
            </div>
            <div id="media" contextmenu="animation-menu">
                <div id="full_screen">
                    <canvas id='canvas' width="850" height="250" style="width:calc(100%);max-width:850px;"></canvas>
                </div>
                <p id="metadata"
                   style="font-size: 16px;text-align: left;max-width: 520px;width: 100%;margin: auto;margin-top: 10px;margin-bottom: 5px;padding-left:15px;"></p>
            </div>
        </div>
    </div>
    <div style="display:table-row;width:100%;height:inherit;min-height:250px;">
        <div style="height: 100%;display:table-cell;position:relative;width:100%;">
            <ol id="track"
                style="display:inline-block;width:calc(100%);max-width:520px;text-align:left;padding-bottom: 5px;position:absolute;top:0;bottom:0;left: calc(50% - 260px)"></ol>
        </div>
    </div>
</div>
<menu id="cache_menu" type="context">
    <menuitem id="vk_cache" label="Cache this song" onclick="vkcache(this.dataset.self)"></menuitem>
    <menuitem id="vk_cache_delete" label="Remove from cache" onclick="vkcache_delete(this)"></menuitem>
    <menuitem id="vk_download" label="Download this song" onclick="vkdownload(this)"></menuitem>
</menu>
<menu id="animation-menu" type="context">
    <menu label="Animation">
        <menuitem label="Spectrum" onclick="SelectAnimation(0)"></menuitem>
        <menuitem label="Spectrum with inverse" onclick="SelectAnimation(1)"></menuitem>
    </menu>
</menu>
<menu id="custom_menu" type="context">
    <menuitem id="custom_remove" label="Remove" onclick="CustomRemove(this.dataset.self)"></menuitem>
    <menuitem id="custom_edit" label="Edit" onclick="CustomEdit(this.dataset.self)"></menuitem>
</menu>
<script type="text/javascript">
    $(function () {
        $.contextMenu('html5');
        $("body").on("keyup", function (event) {
            switch (event.key) {
                case "MediaTrackNext":
                    nextTrack(true);
                    break;
                case "MediaTrackPrevious":
                    prevTrack(true);
                    break;
                case "MediaNextTrack":
                    nextTrack(true);
                    break;
                case "MediaPreviousTrack":
                    prevTrack(true);
                    break;
                case "MediaPlayPause":
                    manage.click();
                    break;
                case "MediaPlay":
                    manage.click();
                    break;
            }
        })
    });

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function getCoords(elem) {
        // (1)
        var box = elem.getBoundingClientRect();

        var body = document.body;
        var docEl = document.documentElement;

        // (2)
        var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

        // (3)
        var clientTop = docEl.clientTop || body.clientTop || 0;
        var clientLeft = docEl.clientLeft || body.clientLeft || 0;

        // (4)
        var top = box.top + scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        return {
            top: top,
            left: left,
            bottom: top + elem.clientHeight,
            right: left + elem.clientWidth
        };
    }

    function isElementInView(element, container) {
        container_coords = getCoords(container);
        elem_coords = getCoords(element);
        return (elem_coords.top >= container_coords.top && elem_coords.bottom <= container_coords.bottom);
    }

    function getElementPosition(element, container) {
        container_coords = getCoords(container);
        elem_coords = getCoords(element);
        return (elem_coords.top <= container_coords.top);
    }

    function decodeString(string) {
        if (string && window.TextDecoder) {
            var enc = jschardet.detect(string).encoding;
            if (enc && !enc.match(/ascii/i)) {
                var buffer = new StringView(string, enc).buffer;
                try {
                    var decoder = new TextDecoder(enc);
                    return decoder.decode(buffer);
                } catch (e) {
                    return string;
                }
            }
        }
        return string;
    }

    function addElement(element, index) {
        var div = document.createElement('div');
        div.setAttribute('contextmenu', 'cache_menu');
        div.addEventListener('contextmenu', function (e) {
            if (radio_list.value == 5) {
                vk_cache.dataset.self = index;
                vk_cache_delete.dataset.self = index;
                vk_download.dataset.self = index;
                vk_download.removeAttribute("disabled");
                if (!this.dataset.cached) {
                    vk_cache_delete.setAttribute("disabled", "disabled");
                    if (vk_cache.hasAttribute("disabled"))
                        vk_cache.removeAttribute("disabled");
                } else {
                    if (vk_cache_delete.hasAttribute("disabled"))
                        vk_cache_delete.removeAttribute("disabled");
                    vk_cache.setAttribute("disabled", "disabled");
                }
            } else {
                vk_cache.setAttribute("disabled", "disabled");
                vk_cache_delete.setAttribute("disabled", "disabled");
                vk_download.setAttribute("disabled", "disabled");
            }
        });
        if (radio_list.value == 5 && db) {
            db.transaction("vk_cache").objectStore("vk_cache").get(vk_audios[index].id).onsuccess = function (event) {
                if (event.target.result)
                    div.dataset.cached = true;
            }
        }
        div.dataset.tag = element;
        div.dataset.index = index;
        div.addEventListener('click', function () {
            if (this.dataset.current == 'true') {
                manage.click();
            } else {
                current_track = Number(this.dataset.index);
                changeTrack(current_track);
            }
        });
        var img = document.createElement('img');
        img.className = 'control_img';
        img.src = 'play.svg';
        var wrap_li = document.createElement('div');
        wrap_li.className = "wrap_list";
        var li = document.createElement('li');
        li.className = "playlist";
        li.innerHTML = element;
        track.appendChild(div);
        div.appendChild(img);
        div.appendChild(wrap_li);
        wrap_li.appendChild(li);
    }

    function updateCustomElement(index, pause, change) {
        if (change) {
            var el = document.querySelector('#track > div[data-current="true"] img');
            if (el)
                el.src = 'play.svg';
            el = document.querySelector('#track > div[data-current="true"]');
            if (el)
                el.dataset.current = false;
        }
        el = document.querySelector('#track > div[data-index="' + index + '"]');
        if (el)
            el.dataset.current = true;
        el = document.querySelector('#track > div[data-index="' + index + '"] img');
        if (el) {
            if (pause !== true)
                el.src = 'pause.svg';
            else
                el.src = 'play.svg';
        }
        if (change) {
            el = document.querySelector('#track > div[data-index="' + index + '"]');
            if (el) {
                document.title = el.dataset.title;
            }
        }
    }

    function addCustomElement(index, url, title) {
        var div = document.createElement('div');
        div.setAttribute('contextmenu', 'custom_menu');
        div.addEventListener('contextmenu', function (e) {
            custom_remove.dataset.self = index;
            custom_edit.dataset.self = index;
        });
        div.dataset.url = url;
        div.dataset.index = index;
        div.dataset.title = title;
        div.dataset.current = index == current_track;
        div.addEventListener('click', function () {
            if (this.dataset.current == 'true') {
                manage.click();
            } else {
                current_track = Number(this.dataset.index);
                init_custom_radio(current_track);
            }
        });
        var img = document.createElement('img');
        img.className = 'control_img';
        if (div.dataset.current == 'true')
            img.src = 'pause.svg';
        else
            img.src = 'play.svg';
        var wrap_li = document.createElement('div');
        wrap_li.className = "wrap_list";
        var li = document.createElement('li');
        li.className = "playlist";
        li.innerHTML = title;
        track.appendChild(div);
        div.appendChild(img);
        div.appendChild(wrap_li);
        wrap_li.appendChild(li);
    }

    function detectmob() {
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        } else {
            return false;
        }
    }

    if (!Element.prototype.remove) {
        Element.prototype.remove = function () {
            if (this.parentNode)
                this.parentNode.removeChild(this);
        }
    }
    Element.prototype.insertAfter = function (elem, next) {
        return this.parentNode.insertBefore(elem, next.nextSibling);
    }

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    var playlists = ['online', 'top-40', 'million-of-hits', 'dance']
    var playlist = playlists[radio_list.value];
    var urls = ["download.php?url=http://cast.nrj.in.ua/nrj", "download.php?url=http://cast.nrj.in.ua/nrj_hot", "download.php?url=http://cast.nrj.in.ua/nrj_hits", "download.php?url=http://cast.nrj.in.ua/nrj_party"];
    var url = urls[radio_list.value];
    var audio;
    var status = '0';
    var timer = null;
    var last_playlist_id = null;
    var eq = Array();
    var source;
    var recording = false;
    var recorder;
    var record_url;
    var custom = false;
    var current_track;
    var error_track = null;
    var last_error_count = null;
    var eq_status = 1;
    var compressor;
    var analyser;
    var playlist_checked;
    var files = document.getElementById("files");
    var vk_audios;
    var update_timer;
    var animation_id;
    var capYPositionArray = [];
    var sleep_time = null;
    var sleep_t;
    var inserted_player = false;
    var toggle_player = false;
    var infinity = false;
    var enable_animation = true;
    sleep_t = setInterval(function () {
        if (sleep_time) {
            var current_time = moment();
            if (sleep_time.get("hour") == current_time.get("hour") && sleep_time.get("minute") == current_time.get("minute")) {
                sleep_time = null;
                $("#sleep_timer").val("Sleep");
                var req = new XMLHttpRequest();
                req.open("GET", "suspend.php");
                req.send();
            }
        }
    }, 100);
    $("#sleep_timer").datetimepicker({
        format: 'HH:mm'
    }).on("dp.show", function (e) {
        if (sleep_time)
            $(this).data("DateTimePicker").hide();
        this.value = "Sleep";
        sleep_time = null;
    }).on("dp.change", function (e) {
        this.value = "Sleep (" + this.value + ")";
    }).on("dp.hide", function (e) {
        if (this.value != "Sleep")
            sleep_time = e.date.set({second: 0});
    });
    $("#sleep_timer").val("Sleep");

    function saveCacheState(value) {
        setCookie("Cache_state", value, {expires: 3600 * 24 * 365});
    }

    (function loadCacheState() {
        cache.checked = getCookie("Cache_state") == "1" ? true : false;
    })();
    cache.onchange = function () {
        saveCacheState(cache.checked == true ? "1" : "0");
    }

    function volumeChange() {
        if (audio) {
            audio.volume = Number(volume.value) / 100;
        }
        vol_value.innerHTML = volume.value + '%';
        setCookie("volume", volume.value, {expires: 3600 * 24 * 365});
    }

    volume.oninput = volumeChange;
    volume.onchange = volumeChange;
    if (getCookie('volume')) {
        volume.value = Number(getCookie('volume'));
        vol_value.innerHTML = volume.value + '%';
    }

    function load_files() {
        if (files.files.length) {
            playlist_checked = false;
            if (url)
                URL.revokeObjectURL(url);
            url = URL.createObjectURL(files.files[0]);
            current_track = 0;
            status = '0';
            init();
            track.innerHTML = '';
            select_files.disabled = true;
            getPlaylist();
        }
    }

    function setButtons(pause, focus) {
        var els = document.querySelectorAll('#track > div[data-current="true"] img');
        if (els.length) {
            els[0].src = "play.svg";
            els[0].parentNode.dataset.current = false;
        }
        els = document.querySelectorAll('#track > div:nth-child(' + (current_track + 1) + ') img');
        if (els.length) {
            els[0].src = !pause ? "pause.svg" : "play.svg";
            els[0].parentNode.dataset.current = true;
            if (!playlist_checked)
                playlist_checked = !pause;
            if (radio_list.value == 5) {
                document.title = vk_audios[current_track].artist + " — " + vk_audios[current_track].title;
            } else {
                document.title = els[0].parentNode.dataset.tag;
            }
            if (focus && !isElementInView(els[0], track)) {
                els[0].scrollIntoView(getElementPosition(els[0], track));
            }
        }
    }

    function changeTrack(track, pause) {
        function ret() {
            status = '0';
            setButtons(true, true);
            if (pause) {
                status = '2';
                audio.src = url;
                manage.innerHTML = 'Play';
            } else {
                if (radio_list.value == 5 && cache.checked) {
                    try {
                        db.transaction("vk_cache").objectStore("vk_cache").get(vk_audios[track].id).onsuccess = function (event) {
                            if (!event.target.result) {
                                vkcache(track, true);
                            }
                        }
                    } catch (e) {
                    }
                }
                init();
            }
        }

        if (url)
            URL.revokeObjectURL(url);
        if (radio_list.value == 4) {
            url = URL.createObjectURL(files.files[track]);
            ret();
        } else {
            if (db) {
                try {
                    db.transaction("vk_cache").objectStore("vk_cache").get(vk_audios[track].id).onsuccess = function (event) {
                        if (event.target.result) {
                            url = URL.createObjectURL(event.target.result.blob);
                        } else {
                            url = "vk.php?url=" + vk_audios[track].url;
                        }
                        ret();
                    }
                } catch (e) {
                    url = "vk.php?url=" + vk_audios[track].url;
                    ret();
                }
            } else {
                url = "vk.php?url=" + vk_audios[track].url;
                ret()
            }
        }
    }

    function nextTrack(error) {
        if (!custom)
            return;
        current_track++;
        if (radio_list.value == 4) {
            if (files.files.length <= current_track && !loop.checked && !error) {
                current_track = 0;
                changeTrack(current_track, true);
                return;
            } else if (files.files.length <= current_track) {
                current_track = 0;
            }
        } else {
            if (vk_audios.length <= current_track && !loop.checked && !error) {
                current_track = 0;
                changeTrack(current_track, true);
                return;
            } else if (vk_audios.length <= current_track) {
                current_track = 0;
            }
        }
        changeTrack(current_track);
    }

    function prevTrack(error) {
        if (!custom)
            return;
        current_track--;
        if (radio_list.value == 4) {
            if (current_track < 0 && !loop.checked && !error) {
                current_track = files.files.length - 1;
                changeTrack(current_track, true);
                return;
            } else if (current_track < 0) {
                current_track = files.files.length - 1;
            }
        } else {
            if (current_track < 0 && !loop.checked && !error) {
                current_track = vk_audios.length - 1;
                changeTrack(current_track, true);
                return;
            } else if (current_track < 0) {
                current_track = vk_audios.length - 1;
            }
        }
        changeTrack(current_track);
    }

    function stop() {
        status = '2';
        audio.pause();
        url = '';
        audio.src = '';
        manage.innerHTML = 'Play';
        if (timer) {
            clearInterval(timer);
            timer = null;
        }
    }

    files.onchange = function () {
        load_files();
    }
    select_files.onclick = function () {
        files.click();
    }

    function getToken() {
        if (window.vk_access_token) {
            setCookie("vk_token", window.vk_access_token, {expires: 3600 * 24 * 365});
            window.vk_access_token = undefined;
        }
        var token = getCookie('vk_token');
        if (token)
            return token;
        window.open("login.php", "login", "width=600,height=300,top=" + (screen.height / 2 - 150) + ",left=" + (screen.width / 2 - 300))
    }

    function vk_init() {
        vkcache_getAll();
        return;
        var token = getToken();
        if (!token)
            return;
        var req = new XMLHttpRequest();
        req.open("GET", 'vk.php?token=' + token);
        req.responseType = 'json';
        req.onload = function () {
            if (req.status == 200) {
                var response = req.response;
                if (typeof req.response == 'string') {
                    response = JSON.parse(req.response);
                }
                if (response.error) {
                    setCookie("vk_token", null, {expires: -10});
                    getToken();
                    return;
                }
                vk_audios = response.response.items;
                current_track = 0;
                changeTrack(current_track);
                getPlaylist();
            } else {
                vkcache_getAll();
            }
        }
        req.onerror = function () {
            vkcache_getAll();
        }
        req.send();
    }

    cache_all.onclick = function () {
        if (!caching)
            cacheAll();
        else
            cancelCache();
    }
    clear_cache.onclick = function () {
        if (confirm("Do you want clear cache?"))
            clearCache();
    }

    function insertPlayer(remove) {
        if (remove && !inserted_player) {
            audio.remove();
            return;
        }
        else if (remove) {
            audio.controls = false;
            audio.style.display = 'none';
            return;
        }
        if (!inserted_player) {
            audio.style.maxWidth = "850px";
            audio.style.width = "100%";
            audio.style.outline = 'none';
            audio.style.display = 'block';
            audio.style.marginTop = '5px';
            audio.style.margin = 'auto';
            audio.style.minHeight = "40px";
            audio.controls = true;
            media.appendChild(audio);
        }
        else {
            audio.controls = true;
            audio.style.display = 'block';
        }

        inserted_player = true;
    }

    radio_list.onchange = function (onload) {
        if (this.value < 4)
            infinity = true;
        else infinity = false;
        if (custom)
            track.innerHTML = '';
        if (this.value != 5)
            cancelCache();
        metadata.innerHTML = '';
        custom_add.style.display = 'none';
        cache_block.style.display = "none";
        cache_all.style.display = "none";
        clear_cache.style.display = "none";
        download_playlist.style.display = "none";
        last_playlist_id = null;
        vk_audios = null;
        select_files.disabled = false;
        playlist_checked = false;
        if (custom && url) {
            URL.revokeObjectURL(url);
        }
        reset_file.reset();
        if (this.value == 4 || this.value == 5) {
            track.innerHTML = '';
            if (recording) {
                recorder.stop();
                recorder.clear();
                rec.innerHTML = "Record";
                recording = false;
            }
            custom = true;
            insertPlayer();
            document.title = "Player";
            if (this.value == 4)
                select_files.style.display = "";
            else
                select_files.style.display = "none";
            loop_block.style.display = "inline-block";
            rec.style.display = "none";
            stop();
            if (this.value == 5) {
                cache_block.style.display = "inline-block";
                cache_all.style.display = "";
                clear_cache.style.display = "";
                download_playlist.style.display = "";
                vk_init();
            } else if (onload === true)
                return;
            else
                select_files.click();
        } else {
            select_files.style.display = "none";
            loop_block.style.display = "none";
            rec.style.display = "";
            insertPlayer(true);
            custom = false;
            url = urls[this.value];
            playlist = playlists[this.value];
            status = '0';
            if (radio_list.value != 6) {
                init();
                getPlaylist();
            } else {
                init_custom_radio();
            }
        }
    }
    download_playlist.onclick = function () {
        var win = window.open('playlist.php', 'playlist', "width=900,height=500,top=" + (screen.height / 2 - 250) + ",left=" + (screen.width / 2 - 450) + ",menubar=no,toolbar=no,location=no,status=no,scrollbars=yes");
    }

    function init_custom_radio(index) {
        infinity = false;
        metadata.innerHTML = '';
        if (index !== undefined) {
            stop();
            status = '0';
            current_track = index;
            updateCustomElement(index, false, true);
            url = 'download.php?url=' + document.querySelector('#track > div[data-index="' + index + '"]').dataset.url;
            init();
            getMetaData(url);
            checkLength(url);
            return;
        }
        document.title = 'Custom radio';
        stop();
        status = '0';
        custom_add.style.display = 'inline-block';
        track.innerHTML = '';
        load_custom();
        var el = document.querySelector('#track > div:nth-child(' + (1) + ')');
        if (el) {
            url = 'download.php?url=' + el.dataset.url;
            document.title = el.dataset.title;
        }
        if (url) {
            current_track = 0;
            updateCustomElement(0, false, true);
        } else
            current_track = -1;
        init();
        getMetaData(url);
        checkLength(url);
    }

    function checkLength(u) {
        var source_url = u.substr(url.indexOf('url=') + 4);
        var target_url = u;
        var req = new XMLHttpRequest();
        req.open('GET', 'download.php?test=' + encodeURIComponent(source_url));
        req.responseType = 'json';
        req.onload = function () {
            if (req.status == 200 && target_url === url) {
                if (req.response) {
                    infinity = false;
                    if (!toggle_player)
                        CustomToggle();
                }
                else {
                    if (toggle_player)
                        CustomToggle();
                    infinity = true;
                }
            }
        }
        req.send();
    }

    custom_add.onclick = function () {
        var radio_url = prompt("Radio url");
        if (radio_url !== null && radio_url != '') {
            var radio_title = prompt("Radio title");
            if (radio_title !== null && radio_title != '') {
                save_custom(radio_url, radio_title);
            }
            track.innerHTML = '';
            load_custom();
            var custom_list = getCookie('custom_radio');
            custom_list = JSON.parse(custom_list);
            init_custom_radio(custom_list.length - 1);
        }
    }

    function CustomEdit(index) {
        var self = document.querySelector('#track > div[data-index="' + index + '"]');
        var radio_url = prompt("Radio url", self.dataset.url);
        if (radio_url !== null && radio_url != '') {
            var radio_title = prompt("Radio title", self.dataset.title);
            if (radio_title !== null && radio_title != '') {
                try {
                    var custom_list = getCookie('custom_radio');
                    if (custom_list)
                        custom_list = JSON.parse(custom_list);
                    else
                        custom_list = Array();
                    custom_list.forEach(function (el, i) {
                        if (el.title === radio_title && i != index)
                            throw new Error('Duplicate title');
                    });
                    custom_list[index].url = radio_url;
                    custom_list[index].title = radio_title;
                    setCookie("custom_radio", JSON.stringify(custom_list), {expires: 3600 * 24 * 365});
                } catch (e) {
                    alert(e.message);
                }
            }
            track.innerHTML = '';
            load_custom();
            init_custom_radio(index);
        }
    }

    function CustomToggle() {
        insertPlayer(toggle_player);
        toggle_player = !toggle_player;
    }

    function CustomRemove(index) {
        var custom_list = getCookie('custom_radio');
        custom_list = JSON.parse(custom_list);
        custom_list.splice(index, 1);
        setCookie("custom_radio", JSON.stringify(custom_list), {expires: 3600 * 24 * 365});
        track.innerHTML = '';
        if (index == current_track) {
            stop();
            status = '0';
            current_track = -1;
            document.title = 'Custom radio';
        } else if (index < current_track) {
            current_track--;
        }
        load_custom();
    }

    function load_custom() {
        var custom_list = getCookie('custom_radio');
        if (custom_list) {
            custom_list = JSON.parse(custom_list);
            custom_list.forEach(function (el, index) {
                addCustomElement(index, el.url, el.title);
            });
        }
    }

    function save_custom(url, title) {
        try {
            var custom_list = getCookie('custom_radio');
            if (custom_list)
                custom_list = JSON.parse(custom_list);
            else
                custom_list = Array();
            custom_list.forEach(function (el) {
                if (el.title === title)
                    throw new Error('Duplicate title');
            });
            custom_list.push({url: url, title: title});
            setCookie("custom_radio", JSON.stringify(custom_list), {expires: 3600 * 24 * 365});
        } catch (e) {
            alert(e.message);
        }
    }

    manage.onclick = function () {
        switch (status) {
            case "0":
                status = '2';
                if (!custom) {
                    audio.src = '';
                    if (radio_list.value == 6 && current_track != -1)
                        updateCustomElement(current_track, true);
                    init();
                }
                if (custom) {
                    audio.pause();
                }
                break;
            case "1":
                status = '2';
                if (!custom) {
                    audio.src = '';
                    if (radio_list.value == 6 && current_track != -1)
                        updateCustomElement(current_track, true);
                    init();
                }
                if (custom) {
                    audio.pause();
                }
                break;
            case "2":
                status = '0';
                if (custom)
                    init(true);
                else {
                    if (radio_list.value == 6 && current_track != -1)
                        updateCustomElement(current_track);
                    init();
                }
                break;
        }
    }

    function buildEQ(analyser, compressor, context) {
        var time = 1;
        for (var i = 0; i < 18; i++) {
            var biquadFilter = context.createBiquadFilter();
            eq.push(biquadFilter);
            switch (i) {
                case 0:
                    analyser.connect(eq[i]);
                    break;
                case 17:
                    eq[i - 1].connect(eq[i]);
                    eq[i].connect(compressor);
                    compressor.connect(context.destination);
                    break;
                default:
                    eq[i - 1].connect(eq[i]);
                    break;
            }
            eq[i].type = "peaking";
            //eq[i].gain.value = 0;
            eq[i].gain.setTargetAtTime(0, context.currentTime, time);
            switch (i) {
                case 0:
                    //eq[i].frequency.value = 31;
                    eq[i].frequency.setTargetAtTime(31, context.currentTime, time);
                    //eq[i].Q.value = 31 / 32;
                    eq[i].Q.setTargetAtTime(31 / 32, context.currentTime, time);
                    break;
                case 1:
                    //eq[i].frequency.value = 63;
                    eq[i].frequency.setTargetAtTime(63, context.currentTime, time);
                    //eq[i].Q.value = 63 / 32;
                    eq[i].Q.setTargetAtTime(63 / 32, context.currentTime, time);
                    break;
                case 2:
                    //eq[i].frequency.value = 87;
                    eq[i].frequency.setTargetAtTime(87, context.currentTime, time);
                    //eq[i].Q.value = 87 / 16;
                    eq[i].Q.setTargetAtTime(87 / 16, context.currentTime, time);
                    break;
                case 3:
                    //eq[i].frequency.value = 125;
                    eq[i].frequency.setTargetAtTime(125, context.currentTime, time);
                    //eq[i].Q.value = 125 / 60;
                    eq[i].Q.setTargetAtTime(125 / 60, context.currentTime, time);
                    break;
                case 4:
                    //eq[i].frequency.value = 175;
                    eq[i].frequency.setTargetAtTime(175, context.currentTime, time);
                    //eq[i].Q.value = 175 / 40;
                    eq[i].Q.setTargetAtTime(175 / 40, context.currentTime, time);
                    break;
                case 5:
                    //eq[i].frequency.value = 250;
                    eq[i].frequency.setTargetAtTime(250, context.currentTime, time);
                    //eq[i].Q.value = 250 / 110;
                    eq[i].Q.setTargetAtTime(250 / 110, context.currentTime, time);
                    break;
                case 6:
                    //eq[i].frequency.value = 350;
                    eq[i].frequency.setTargetAtTime(350, context.currentTime, time);
                    //eq[i].Q.value = 350 / 90;
                    eq[i].Q.setTargetAtTime(350 / 90, context.currentTime, time);
                    break;
                case 7:
                    //eq[i].frequency.value = 500;
                    eq[i].frequency.setTargetAtTime(500, context.currentTime, time);
                    //eq[i].Q.value = 500 / 210;
                    eq[i].Q.setTargetAtTime(500 / 210, context.currentTime, time);
                    break;
                case 8:
                    //eq[i].frequency.value = 700;
                    eq[i].frequency.setTargetAtTime(700, context.currentTime, time);
                    //eq[i].Q.value = 700 / 190;
                    eq[i].Q.setTargetAtTime(700 / 190, context.currentTime, time);
                    break;
                case 9:
                    //eq[i].frequency.value = 1000;
                    eq[i].frequency.setTargetAtTime(1000, context.currentTime, time);
                    //eq[i].Q.value = 1000 / 410;
                    eq[i].Q.setTargetAtTime(1000 / 410, context.currentTime, time);
                    break;
                case 10:
                    //eq[i].frequency.value = 1400;
                    eq[i].frequency.setTargetAtTime(1400, context.currentTime, time);
                    //eq[i].Q.value = 1400 / 390;
                    eq[i].Q.setTargetAtTime(1400 / 390, context.currentTime, time);
                    break;
                case 11:
                    //eq[i].frequency.value = 2000;
                    eq[i].frequency.setTargetAtTime(2000, context.currentTime, time);
                    //eq[i].Q.value = 2000 / 810;
                    eq[i].Q.setTargetAtTime(2000 / 810, context.currentTime, time);
                    break;
                case 12:
                    //eq[i].frequency.value = 2800;
                    eq[i].frequency.setTargetAtTime(2800, context.currentTime, time);
                    //eq[i].Q.value = 2800 / 790;
                    eq[i].Q.setTargetAtTime(2800 / 790, context.currentTime, time);
                    break;
                case 13:
                    //eq[i].frequency.value = 4000;
                    eq[i].frequency.setTargetAtTime(4000, context.currentTime, time);
                    //eq[i].Q.value = 4000 / 1610;
                    eq[i].Q.setTargetAtTime(4000 / 1610, context.currentTime, time);
                    break;
                case 14:
                    //eq[i].frequency.value = 5600;
                    eq[i].frequency.setTargetAtTime(5600, context.currentTime, time);
                    //eq[i].Q.value = 5600 / 1590;
                    eq[i].Q.setTargetAtTime(5600 / 1590, context.currentTime, time);
                    break;
                case 15:
                    //eq[i].frequency.value = 8000;
                    eq[i].frequency.setTargetAtTime(8000, context.currentTime, time);
                    //eq[i].Q.value = 8000 / 3210;
                    eq[i].Q.setTargetAtTime(8000 / 3210, context.currentTime, time);
                    break;
                case 16:
                    //eq[i].frequency.value = 11200;
                    eq[i].frequency.setTargetAtTime(11200, context.currentTime, time);
                    //eq[i].Q.value = 11200 / 3190;
                    eq[i].Q.setTargetAtTime(11200 / 3190, context.currentTime, time);
                    break;
                case 17:
                    //eq[i].frequency.value = 16000;
                    eq[i].frequency.setTargetAtTime(16000, context.currentTime, time);
                    //eq[i].Q.value = 16000 / 6410;
                    eq[i].Q.setTargetAtTime(16000 / 6410, context.currentTime, time);
                    break;
            }
        }
    }

    function getMetaData(u) {
        if (!infinity)
            return;
        var source_url = u.substr(url.indexOf('url=') + 4);
        var target_url = u;
        var req = new XMLHttpRequest();
        req.open('GET', 'download.php?getmetadata=1&url=' + source_url);
        req.onload = function () {
            if (req.status == 200 && target_url === url && req.response) {
                if (metadata.innerHTML !== req.response) {
                    metadata.innerHTML = req.response;
                    document.title = req.response;
                }
            }
        }
        req.send();
    }

    function init(custom_init) {
        if (status == '2') {
            manage.innerHTML = 'Play';
            return;
        }
        if (url)
            manage.innerHTML = "Pending...";
        if (!audio) {
            audio = new Audio(url);
            if (window.AudioContext) {
                var context = new AudioContext();
                analyser = context.createAnalyser();
                source = analyser.context.createMediaElementSource(audio);
                compressor = context.createDynamicsCompressor();
                source.connect(analyser);
                buildEQ(analyser, compressor, context);
                if (enable_animation) {
                    var drawMeter = function () {
                        var that = this,
                            margin = 2,
                            canvas = document.getElementById('canvas'),
                            cwidth = canvas.width,
                            meterWidth = 10, //width of the meters in the spectrum
                            gap = 2, //gap between meters
                            capHeight = 2,
                            cheight = canvas.height - capHeight,
                            capStyle = '#aaa',
                            meterNum = cwidth / (meterWidth + margin), //count of the meters
                            ctx = canvas.getContext('2d'),
                            gradient = ctx.createLinearGradient(0, 0, 0, canvas.height);
                        gradient.addColorStop(1, '#0f0');
                        gradient.addColorStop(0.5, '#ff0');
                        gradient.addColorStop(0, '#f00');
                        var array = new Uint8Array(analyser.frequencyBinCount);
                        analyser.getByteFrequencyData(array);
                        var step = Math.round((array.length - 300) / meterNum); //sample limited data from the total array
                        ctx.clearRect(0, 0, cwidth, cheight);
                        for (var i = 0; i < meterNum; i++) {
                            var value = status == '1' ? array[i * (step)] / 256 * canvas.height : 0;
                            if (capYPositionArray.length < Math.round(meterNum)) {
                                capYPositionArray.push(value);
                            }
                            ;
                            ctx.fillStyle = capStyle;
                            //draw the cap, with transition effect
                            if (value < capYPositionArray[i]) {
                                ctx.fillRect(i * (meterWidth + margin), cheight - (--capYPositionArray[i]), meterWidth, capHeight);
                            } else {
                                ctx.fillRect(i * (meterWidth + margin), cheight - value, meterWidth, capHeight);
                                capYPositionArray[i] = value;
                            }
                            ;
                            ctx.fillStyle = gradient; //set the filllStyle to gradient for a better look
                            ctx.fillRect(i * (meterWidth + margin) /*meterWidth+gap*/, cheight - value + capHeight, meterWidth, cheight); //the meter
                        }
                        if (!animation_id)
                            that.animationId = requestAnimationFrame(drawMeter);
                        else
                            that.animationId = requestAnimationFrame(drawMeter_with_inverse);
                    }
                    var drawMeter_with_inverse = function () {
                        var that = this,
                            margin = 4,
                            canvas = document.getElementById('canvas'),
                            cwidth = canvas.width,
                            meterWidth = 10, //width of the meters in the spectrum
                            capHeight = 2,
                            meterNum = cwidth / (meterWidth + margin), //count of the meters
                            ctx = canvas.getContext('2d'),
                            gradient = ctx.createLinearGradient(0, 0, 0, canvas.height);
                        gradient.addColorStop(1, '#f00');
                        gradient.addColorStop(0.75, '#ff0');
                        gradient.addColorStop(0.5, '#0f0');
                        gradient.addColorStop(0.25, '#ff0');
                        gradient.addColorStop(0, '#f00');
                        var array = new Uint8Array(analyser.frequencyBinCount);
                        analyser.getByteFrequencyData(array);
                        var step = Math.round((array.length - 300) / meterNum); //sample limited data from the total array
                        ctx.clearRect(0, 0, cwidth, canvas.height);
                        for (var i = 0; i < meterNum; i++) {
                            var value = status == '1' ? array[i * (step)] / 256 * canvas.height : 0;
                            ctx.fillStyle = '#0f0';
                            ctx.fillRect(i * (meterWidth + margin), (canvas.height - capHeight) / 2, meterWidth, capHeight);
                            ctx.fillStyle = gradient; //set the filllStyle to gradient for a better look
                            ctx.fillRect(i * (meterWidth + margin) /*meterWidth+gap*/, (canvas.height - value) / 2, meterWidth, value); //the meter
                        }
                        if (!animation_id)
                            that.animationId = requestAnimationFrame(drawMeter);
                        else
                            that.animationId = requestAnimationFrame(drawMeter_with_inverse);
                    }
                    if (!animation_id)
                        this.animationId = requestAnimationFrame(drawMeter);
                    else
                        this.animationId = requestAnimationFrame(drawMeter_with_inverse);
                }
                else {
                    canvas.remove();
                }
            } else {
                rec.disabled = true;
                toggle_eq.disabled = true;
                canvas.remove();
            }
            audio.volume = Number(volume.value) / 100;
            audio.onvolumechange = function () {
                volume.value = audio.volume * 100;
                volumeChange();
            }
            audio.onended = function () {
                if (!custom)
                    return;
                nextTrack();
            }
            audio.onerror = function () {
                if (status == '2') {
                    if (audio.src != "" && audio.src != window.location.href)
                        audio.src = "";
                    return;
                }
                if (custom && url == "" && (radio_list.value != 5 || !vk_audios)) {
                    status = '0';
                    return;
                }
                if (custom) {
                    if (error_track !== null && error_track == current_track) {
                        error_track = null;
                        manage.innerHTML = 'Play';
                        return;
                    }
                    if (error_track === null)
                        error_track = current_track;
                    if (status != '2') {
                        status = '0';
                    }
                    nextTrack(true);
                    return;
                }
                if (!custom) {
                    if (!infinity)
                        window.audio_position = audio.currentTime;
                    if (!last_error_count) {
                        last_error_count = 0;
                    }
                    last_error_count++;
                    if (last_error_count >= 10) {
                        setTimeout(function () {
                            if (status != '0')
                                return;
                            last_error_count = 0;
                            init();
                        }, 10000);
                        return;
                    }
                }
                if (status != '2') {
                    status = '0';
                    init();
                }
            }
            audio.onplaying = function () {
                window.audio_position = undefined;
                error_track = null;
                last_error_count = null;
                if (custom) {
                    manage.innerHTML = "Pause";
                    setButtons();
                } else
                    manage.innerHTML = "Stop";
                status = '1';
            }
            audio.onabort = function () {
                if (status != '2')
                    status = '0';
                else {
                    if (error_track === null)
                        manage.innerHTML = "Play";
                }
            }
            audio.onseeking = function () {
                if (status == '2') {
                    manage.click();
                }
            }
            audio.onpause = function () {
                if (audio.seeking)
                    return;
                if (custom && error_track === null)
                    manage.innerHTML = "Play";
                if (custom) {
                    setButtons(true);
                    return;
                }
                if (status == '2')
                    audio.src = '';
            }
            audio.ontimeupdate = function () {
                if (update_timer) {
                    clearTimeout(update_timer);
                    update_timer = null;
                }
                if (custom) {
                    update_timer = setTimeout(function () {
                        if (custom && status == '1') {
                            if (audio.duration - audio.currentTime < 0.1) {
                                nextTrack();
                            }
                        }
                    }, 500);
                }
                if (timer) {
                    clearInterval(timer);
                    timer = null;
                }
                if (custom)
                    return;
                timer = setInterval(function () {
                    if (last_error_count === null && infinity)
                        init();
                }, 10000);
            }
        } else {
            if (window.AudioContext)
                analyser.context.resume();
            if (custom_init !== true || audio.src == "")
                audio.src = url;
        }
        var play = audio.play();
        if (!infinity && window.audio_position) {
            audio.currentTime = window.audio_position;
        }
        if (window.Promise && play instanceof Promise) {
            play.catch(function (error) {
                if (error.name === 'NotAllowedError') {
                    status = '2';
                    if (!custom)
                        audio.src = '';
                    init();
                }
            });
        }
    }

    function getTracks(tracks) {
        var list = '';
        for (var i = 0; i < tracks.length; i++) {
            var artists = "";
            for (var j = 0; j < tracks[i].artists.length; j++) {
                artists += tracks[i].artists[j].name + " ";
            }
            list += "<li><b>" + tracks[i].time + '</b> ' + artists + "— " + tracks[i].song + "</li>";
        }
        track.innerHTML = list;
    }

    function getPlaylist() {
        if (custom) {
            if (radio_list.value == 4) {
                function getID3(index, ie) {
                    if (!ie) {
                        jsmediatags.read(files.files[index], {
                            onSuccess: function (tag) {
                                //console.log(tag);
                                var el;
                                var title = decodeString(tag.tags.title);
                                if (title) {
                                    var artist = decodeString(tag.tags.artist);
                                    if (artist) {
                                        el = artist + " — " + title;
                                    } else
                                        el = title;
                                } else
                                    el = files.files[index].name;
                                addElement(el, index);
                                if (!playlist_checked) {
                                    setButtons(status != '1');
                                }
                                if (++index < files.files.length)
                                    getID3(index);
                                else
                                    select_files.disabled = false;
                            },
                            onError: function (error) {
                                addElement(files.files[index].name, index);
                                if (!playlist_checked) {
                                    setButtons(status != '1');
                                }
                                if (++index < files.files.length)
                                    getID3(index);
                                else
                                    select_files.disabled = false;
                            }
                        });
                    } else {
                        addElement(files.files[index].name, index);
                        if (++index < files.files.length)
                            setTimeout(getID3, 0, index, ie);
                        else
                            select_files.disabled = false;
                    }
                }

                if (files.files.length) {
                    if (FileReader.prototype.readAsBinaryString)
                        getID3(0);
                    else
                        getID3(0, true);
                }
            } else if (radio_list.value == 5) {
                function getID3(index) {
                    var el;
                    el = vk_audios[index].artist + " — " + vk_audios[index].title;
                    addElement(el, index);
                    if (!playlist_checked) {
                        setButtons(status != '1');
                    }
                    if (++index < vk_audios.length)
                        setTimeout(getID3, 0, index);
                }

                getID3(0);
            }
        } else {
            var req = new XMLHttpRequest();
            req.open("GET", 'download.php');
            req.responseType = 'json';
            req.onload = function () {
                if (custom)
                    return;
                if (req.response || req.responseText) {
                    var response;
                    if (req.response)
                        response = req.response;
                    else if (req.responseText)
                        response = JSON.parse(req.responseText);
                    if (typeof response == 'string') {
                        response = JSON.parse(req.response);
                    }
                    if (last_playlist_id && last_playlist_id == response.playlists[playlist][0].id)
                        return;
                    var artists = "";
                    for (var i = 0; i < response.playlists[playlist][0].artists.length; i++) {
                        artists += response.playlists[playlist][0].artists[i].name + " ";
                    }
                    var song = artists + '— ' + response.playlists[playlist][0].song;
                    document.title = song;
                    last_playlist_id = response.playlists[playlist][0].id;
                    getTracks(response.playlists[playlist]);
                }
            }
            req.send();
        }
    }

    if (detectmob() || !window.AudioContext)
        enable_animation = false;
    if (radio_list.value != 6) {
        if (radio_list.value < 4)
            infinity = true;
        init();
    }
    else {
        init();
        init_custom_radio();
    }
    if (radio_list.value == 4 || radio_list.value == 5) {
        radio_list.onchange(true);
    }
    if (window.AudioContext)
        loadEQData();
    setInterval(function () {
        if (!custom) {
            if (radio_list.value != 6)
                getPlaylist();
            else
                getMetaData(url);
        }
    }, 10000);
    if (!custom && radio_list.value != 6)
        getPlaylist();

    function loadEQData() {
        var eq = getCookie('EQ');
        if (eq) {
            eq = '["7.2","7.2","7.2","6.6","3.9","0","-6.3","-2.1","-1.5","0","1.2","2.1","4.2","6","7.8","8.4","9","9"]';
        }
        if (eq) {
            eq = JSON.parse(eq);
            for (var i = 0; i < eq.length; i++) {
                document.getElementById("eq_" + (i + 1)).value = eq[i];
                //window.eq[i].gain.value = Number(eq[i]);
                window.eq[i].gain.setTargetAtTime(Number(eq[i]), window.eq[i].context.currentTime, 2);
            }
        }
        saveEQData();
    }

    function saveEQData() {
        var data = Array();
        for (var i = 0; i < 18; i++) {
            data.push(document.getElementById("eq_" + (i + 1)).value)
        }
        setCookie("EQ", JSON.stringify(data), {expires: 3600 * 24 * 365});
    }

    var eqs = document.getElementsByClassName("eq");
    for (var i = 0; i < eqs.length; i++) {
        eqs[i].oninput = function () {
            if (eq.length) {
                var el = Number(this.id.slice(3)) - 1;
                //eq[el].gain.value = Number(this.value);
                eq[el].gain.setTargetAtTime(Number(this.value), eq[el].context.currentTime, 1);
            }
            saveEQData();
        }
    }
    reset_eq.onclick = function () {
        for (var i = 0; i < 18; i++) {
            document.getElementById("eq_" + (i + 1)).value = 0;
        }
        saveEQData();
        loadEQData();
    }
    if (enable_animation) {
        canvas.ondblclick = function () {
            var elem = canvas;
            if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.mozRequestFullScreen) {
                    elem.mozRequestFullScreen();
                } else if (elem.webkitRequestFullscreen) {
                    elem.webkitRequestFullscreen();
                }
            } else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
            }
        }
        var eFullscreenName = function () {
            if ('onfullscreenchange' in document)
                return 'fullscreenchange';
            if ('onmozfullscreenchange' in document)
                return 'mozfullscreenchange';
            if ('onwebkitfullscreenchange' in document)
                return 'webkitfullscreenchange';
            if ('onmsfullscreenchange' in document)
                return 'MSFullscreenChange';
            return false;
        }();
        var f_timer = null;

        function hide_cursor() {
            canvas.style.cursor = "none";
        }

        canvas.onmousemove = function () {
            if (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement) {
                if (f_timer) {
                    clearInterval(f_timer);
                    f_timer = null;
                }
                canvas.style.cursor = "default";
                f_timer = setInterval(hide_cursor, 1000);
            }
        }
        if (eFullscreenName)
            document.addEventListener(eFullscreenName, function () {
                if (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement) {
                    canvas.width = screen.width;
                    canvas.height = screen.height;
                    capYPositionArray = [];
                    canvas.style.background = 'url("background.jpg") no-repeat scroll 0px 0px';
                    canvas.style.backgroundSize = 'cover';
                    canvas.style.maxWidth = 'none';
                    if (!f_timer) {
                        f_timer = setInterval(hide_cursor, 1000);
                    }
                } else {
                    canvas.width = 850;
                    canvas.height = 250;
                    capYPositionArray = [];
                    canvas.style.background = 'transparent';
                    canvas.style.backgroundSize = 'none';
                    canvas.style.maxWidth = '850px';
                    if (f_timer) {
                        clearInterval(f_timer);
                        f_timer = null;
                        canvas.style.cursor = "default";
                    }
                }
            }, false);
    }

    function forceDownload(blob, filename) {
        if (record_url)
            URL.revokeObjectURL(record_url);
        record_url = (window.URL || window.webkitURL).createObjectURL(blob);
        var link = window.document.createElement('a');
        link.href = record_url;
        link.download = filename || 'output.wav';
        var evt = document.createEvent('MouseEvents');
        evt.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(evt);
    }

    rec.onclick = function () {
        if (!recording) {
            recorder = new Recorder(source, {type: "audio/mp3", bitRate: 320});
            recorder.record();
            this.innerHTML = "Stop recording";
            recording = true;
        } else {
            this.innerHTML = "Processing";
            this.setAttribute("disabled", "disabled");
            recording = false;
            recorder.stop();
            var that = this;
            recorder.export(function (b) {
                that.removeAttribute("disabled");
                that.innerHTML = "Record";
                forceDownload(b, "record.mp3");
                recorder.clear();
            });
        }
    }
    toggle_eq.onclick = function () {
        if (this.dataset.show == "0") {
            document.querySelector(".wrap_eq").style.display = "block";
            this.dataset.show = "1";
            this.innerHTML = "Hide equalizer";
        } else {
            document.querySelector(".wrap_eq").style.display = "none";
            this.dataset.show = "0";
            this.innerHTML = "Show equalizer";
        }
    }
    manage_eq.onclick = function () {
        if (eq_status) {
            analyser.disconnect(eq[0]);
            compressor.disconnect(analyser.context.destination);
            analyser.connect(analyser.context.destination);
            eq_status = 0;
            this.value = "On";
        } else {
            analyser.disconnect(analyser.context.destination);
            analyser.connect(eq[0]);
            compressor.connect(analyser.context.destination);
            eq_status = 1;
            this.value = "Off";
        }
        setCookie('eq_disabled', eq_status === 0, {expires: 3600 * 24 * 365});
    }

    function SelectAnimation(id) {
        animation_id = id;
        setCookie('animation_id', id, {expires: 3600 * 24 * 365});
    }

    (function getAnimationId() {
        if (getCookie('animation_id'))
            animation_id = getCookie('animation_id') !== '0';
    })();
    (function getEQState() {
        if (window.AudioContext) {
            if (getCookie('eq_disabled') && getCookie('eq_disabled') !== 'false') {
                manage_eq.click();
            }
        }
    })();
</script>
</body>
</html>