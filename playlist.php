<!DOCTYPE html>
<html>
    <head>
        <title>Playlist</title>
        <meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="bootstrap.min.js"></script>
        <script type="text/javascript" src="playlist.js"></script>
        <script type="text/javascript" src="cache.js"></script>
        <link rel="stylesheet" href="bootstrap.min.css" />
        <style type="text/css">
            html, body{
                height: 100%;
            }
            body{
                background-size: unset;
                background-repeat: repeat;
            }
        </style>
    </head>
    <body>
        <div style="margin: auto;text-align: center;height: 100%;padding-top: 10px;padding-bottom: 10px;min-height: 250px;">
            <div style="width: 575px;max-width:100%; margin: auto;text-align: left;padding-left: 10px;height: 20px;margin-bottom: 10px;position: relative;">
                <input id="all" name="all" style="display: inline-block;vertical-align: middle;margin-top: 0;" type="checkbox">
                <label for="all" style="display: inline-block;margin-left: 10px;vertical-align: middle;margin-bottom: 0;">Select all tracks</label>
                <button id="download" style="display: inline-block;vertical-align: middle;position: absolute;right: 0;">Download</button>
            </div>
            <ol style="display: inline-block; width:575px; max-width: 100%; text-align: left;padding-left: 25px;max-height: calc(100% - 30px );overflow: auto;margin: 0;" id="track">
            </ol>
        </div>
    </body>
</html>