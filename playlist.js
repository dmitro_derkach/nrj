function init_playlist() {
    var vk_audios = window.opener.vk_audios;
    var style = document.createElement("style");
    var downloads = [];
    var downloading = false;
    download.disabled = true;
    style.innerHTML = window.opener.document.getElementsByTagName("style")[0].innerHTML;
    style.type = "text/css";
    document.getElementsByTagName("head")[0].appendChild(style);
    function countDownload() {
        if (downloading)
            return;
        if (document.querySelectorAll('#track > div input:checked').length) {
            download.disabled = false;
            download.innerHTML = "Download (" + document.querySelectorAll('#track > div input:checked').length + ")";
        } else {
            download.disabled = true;
            download.innerHTML = "Download";
        }
    }
    function cancelDownload() {
        downloading = false;
        countDownload();
    }
    download.onclick = function () {
        if (downloading) {
            cancelDownload();
            return;
        }
        downloading = true;
        downloads = [];
        for (var i = 0; i < document.querySelectorAll('#track > div input:checked').length; i++) {
            downloads.push(Number(document.querySelectorAll('#track > div input:checked')[i].parentNode.dataset.index));
        }
        var req = new XMLHttpRequest();
        req.open("POST", window.opener.location.href + "downloads.php");
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var params = "op=flush";
        req.responseType = 'json';
        req.onload = function () {
            if (req.response) {
                (function upload(index, error) {
                    if (index >= downloads.length || !downloading) {
                        download.disabled = true;
                        download.innerHTML = "Processing...";
                        (function Download(index, part) {
                            var req = new XMLHttpRequest();
                            req.open("POST", window.opener.location.href + "downloads.php");
                            var params = new FormData();
                            params.append("op", "create");
                            if (index && part) {
                                params.append("index", index);
                                params.append("part", part);
                            }
                            req.responseType = "json";
                            req.onload = function () {
                                var link = window.opener.document.createElement('a');
                                link.href = window.opener.location.href + "downloads.php?op=download&filename=" + req.response.filename;
                                var evt = window.opener.document.createEvent('MouseEvents');
                                evt.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                                link.dispatchEvent(evt);
                                if (!req.response.success) {
                                    Download(req.response.index, req.response.part + 1);
                                } else
                                    cancelDownload();
                            }
                            req.send(params);
                        })();
                        return;
                    }
                    if (error >= 3) {
                        upload(index + 1, 0);
                        return;
                    }
                    download.innerHTML = "Cancel download (" + (index + 1) + "\\" + downloads.length + ")";
                    db.transaction("vk_cache").objectStore("vk_cache").get(vk_audios[downloads[index]].id).onsuccess = function (event) {
                        req = new XMLHttpRequest();
                        req.open("POST", window.opener.location.href + "downloads.php");
                        req.responseType = "json";
                        req.onload = function () {
                            if (req.response)
                                upload(index + 1, 0);
                            else
                                upload(index, error + 1);
                        }
                        if (event.target.result) {
                            params = new FormData();
                            params.append("op", "addFile");
                            if (window.navigator.userAgent.indexOf("Edge") != -1)
                                params.append("data\"; filename=\"blob\"", event.target.result.blob);
                            else
                                params.append("data", event.target.result.blob);
                            params.append("filename", document.querySelector('#track > div[data-index="' + downloads[index] + '"]').dataset.tag + ".mp3");
                            req.send(params);
                        } else {
                            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                            params = "op=addLink&link=" + encodeURIComponent(vk_audios[downloads[index]].url) + "&filename=" + encodeURIComponent(document.querySelector('#track > div[data-index="' + downloads[index] + '"]').dataset.tag + ".mp3");
                            req.send(params);
                        }
                    }
                })(0, 0);
            }
        }
        req.send(params);
    }
    function setAll() {
        all.indeterminate = (document.querySelectorAll('#track > div input:checked').length && document.querySelectorAll('#track > div input:checked').length !== document.querySelectorAll('#track > div input').length) ? true : false;
        all.checked = document.querySelectorAll('#track > div input:checked').length == document.querySelectorAll('#track > div input').length;
        countDownload();
    }
    document.querySelector("label").onclick = function () {
        all.checked = !all.checked;
        all.indeterminate = false;
        all.onchange();
        return false;
    }
    all.onchange = function () {
        for (var i = 0; i < document.querySelectorAll('#track > div').length; i++) {
            document.querySelectorAll('#track > div')[i].querySelector("input").checked = all.checked;
            document.querySelectorAll('#track > div')[i].dataset.selected = all.checked ? "true" : "false";
        }
        countDownload();
    }
    if (db) {
        (function add(index) {
            if (index >= vk_audios.length)
                return;
            db.transaction("vk_cache").objectStore("vk_cache").get(vk_audios[index].id).onsuccess = function (event) {
                if (event.target.result || vk_audios[index].url) {
                    var div = document.createElement('div');
                    if (event.target.result)
                        div.dataset.cached = true;
                    div.dataset.tag = vk_audios[index].artist + " - " + vk_audios[index].title;
                    div.dataset.index = index;
                    div.style.paddingLeft = "10px";
                    div.addEventListener('click', function (e) {
                        if (!e.shiftKey && !e.ctrlKey) {
                            var els = document.querySelectorAll('#track > div input:checked');
                            for (var i = 0; i < els.length; i++) {
                                if (els[i].parentNode.dataset.index == div.dataset.index)
                                    continue;
                                els[i].parentNode.dataset.selected = "false";
                                els[i].checked = false;
                            }
                        }
                        if (e.target instanceof HTMLInputElement != true) {
                            document.querySelector('#track > div[data-index="' + div.dataset.index + '"] input').checked = !document.querySelector('#track > div[data-index="' + div.dataset.index + '"] input').checked;
                            if (document.querySelector('#track > div[data-index="' + div.dataset.index + '"] input').checked) {
                                div.dataset.selected = "true";
                            } else {
                                div.dataset.selected = "false";
                            }
                        }
                        if (e.shiftKey) {
                            var checked = div.querySelector("input").checked;
                            if (document.querySelectorAll('#track > div input:checked').length && Number(document.querySelectorAll('#track > div input:checked')[0].parentNode.dataset.index) < Number(div.dataset.index)) {
                                for (var i = Number(div.dataset.index) - 1; i >= 0; i--) {
                                    var el = document.querySelector('#track > div[data-index="' + i + '"]');
                                    if (el) {
                                        if (el.querySelector("input").checked == checked)
                                            break;
                                        el.querySelector("input").checked = checked;
                                        el.dataset.selected = checked === true ? "true" : "false";
                                    }
                                }
                            }
                        }
                        setAll();
                    });
                    var checkbox = document.createElement('input');
                    checkbox.type = "checkbox";
                    checkbox.style.margin = '0';
                    checkbox.style.verticalAlign = 'middle';
                    checkbox.onchange = function (e) {
                        div.dataset.selected = document.querySelector('#track > div[data-index="' + div.dataset.index + '"] input').checked ? "true" : "false";
                        setAll();
                    }
                    var wrap_li = document.createElement('div');
                    wrap_li.className = "wrap_list";
                    wrap_li.style.maxWidth = "calc(100% - 30px)";
                    var li = document.createElement('li');
                    li.className = "playlist";
                    li.innerHTML = vk_audios[index].artist + " &mdash; " + vk_audios[index].title;
                    ;
                    track.appendChild(div);
                    div.appendChild(checkbox);
                    div.appendChild(wrap_li);
                    wrap_li.appendChild(li);
                }
                add(index + 1);
            }
        })(0);
    }
}