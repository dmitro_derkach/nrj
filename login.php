<!DOCTYPE html>
<html>
    <head>
        <title>Playlist</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <script type="text/javascript" src="jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap.min.css" />
        <style type="text/css">
            html, body{
                height: 100%;
            }
            body{
                background-size: unset;
                background-repeat: repeat;
            }
        </style>
    </head>
    <body>
        <div style="margin:auto;display:table;height: 100%;padding-top: 10px;padding-bottom: 10px;width:275px;max-width: 100%;">
            <div style="display:table-cell;vertical-align: middle;">
                <div>
                    <label for="login">VK login</label>
                </div>
                <div>
                   <input type="text" id="login" style="width:100%;" /> 
                </div> 
                <div style="margin-top:5px;">
                    <label for="password">VK password</label>
                </div>
                <div>
                   <input type="password" id="password" style="width:100%;" /> 
                </div> 
                <div id="captcha_block" style="display:none">
                    <div style="margin-top:5px;">
                        <label for="captcha">Captcha</label>
                    </div>
                    <div style="margin-top:5px;">
                        <img src="#" id="captcha_img" />
                    </div>
                    <div style="margin-top: 5px;">
                       <input type="text" id="captcha" style="width:100%;" /> 
                    </div> 
                </div>
                <div style="margin-top: 5px">
                    <input type="button" value="Sign in" id="sign_in" style="width:100%;" />
                </div>
            </div>
        </div>
        <script type="text/javascript">
            window.opener.vk_access_token = undefined;
            var style = document.createElement("style");
            style.innerHTML = window.opener.document.getElementsByTagName("style")[0].innerHTML;
            style.type = "text/css";
            document.getElementsByTagName("head")[0].appendChild(style);
            sign_in.onclick = function(){
                var req = new XMLHttpRequest();
                var captcha_params = '';
                if (captcha_block.dataset.captcha_sid){
                    captcha_params = "&captcha_sid=" + encodeURIComponent(captcha_block.dataset.captcha_sid) + "&captcha_key=" + encodeURIComponent(captcha.value);
                }
                req.open("GET", "vk.php?authorize=1&login=" + encodeURIComponent(login.value) + "&password=" + encodeURIComponent(password.value) + captcha_params);
                req.responseType = 'json';
                req.onload = function(){
                    if (req.status == 500){
                        alert("Network error");
                    }
                    var response = req.response;
                    if (typeof req.response == 'string') {
                        response = JSON.parse(req.response);
                    }
                    if (response.error && response.error == "need_captcha"){
                        captcha_block.style.display = "block";
                        captcha_block.dataset.captcha_sid = response.captcha_sid;
                        captcha_img.src = response.captcha_img;
                        return;
                    }
                    if (response.error && response.error == "invalid_client"){
                        captcha_block.style.display = "none";
                        captcha_block.dataset.captcha_sid = undefined;
                        alert("Invalid username or password");
                        return;
                    }
                    if (response.access_token){
                        window.opener.vk_access_token = response.access_token;
                        window.opener.vk_init();
                        window.close();
                    }
                }
                req.onerror = function(){
                    alert("Network error");
                }
                req.send();
            }
        </script>
    </body>
</html>
