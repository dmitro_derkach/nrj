<?php

if (!function_exists('getallheaders')) {
    function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
        if (substr($name, 0, 5) == 'HTTP_') {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
    }
}

stream_context_set_default(array(
    'http' => array(
        'timeout' => 10,
    )
));

function getRedirectUrl($url) {
    stream_context_set_default(array(
        'http' => array(
            'method' => 'HEAD',
        )
    ));
    $headers = @get_headers($url, 1);
    stream_context_set_default(array(
        'http' => array(
            'method' => 'GET'
        )
    ));
    if ($headers !== false && isset($headers['Location'])) {
        return $headers['Location'];
    }
    return false;
}

if (isset($_GET['getmetadata'])) {

    function getMp3StreamTitle($streamingUrl, $interval = 0, $offset = 0, $headers = true) {
        $redirect = getRedirectUrl($streamingUrl);
        if ($redirect)
            $streamingUrl = $redirect;
        $needle = 'StreamTitle=';
        $ua = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36';

        $opts = [
            'http' => [
                'method' => 'GET',
                'header' => 'Icy-MetaData: 1',
                'user_agent' => $ua
            ]
        ];
        stream_context_set_default($opts);

        if (($headers = @get_headers($streamingUrl)))
            foreach ($headers as $h)
                if (strpos(strtolower($h), 'icy-metaint') !== false && ($interval = explode(':', $h)[1]))
                    break;

        $context = stream_context_create($opts);

        if ($stream = @fopen($streamingUrl, 'r', false, $context)) {
            $buffer = @stream_get_contents($stream, ord(@stream_get_contents($stream, 1, $interval)) * 16, $interval + 1);
            fclose($stream);
            
            if ($buffer){
                if (strpos($buffer, $needle) !== false) {
                    $title = explode($needle, $buffer)[1];
                    return substr($title, 1, strpos($title, "';", 1) - 1);
                }
            } else
                http_response_code(500);
        } else
            http_response_code(500);
    }

    echo getMp3StreamTitle($_GET['url']);
    exit;
}

if (isset($_GET['url'])) {
    $url = getRedirectUrl($_GET['url']);
    if (!$url)
        $url = $_GET['url'];
	$all_headers = getallheaders();
	$headers = array();

	foreach ($all_headers as $n => $v){
		if (stripos($n, "User-Agent") === false)
            if(stripos($n, "HOST") === false)
			$headers[] = "$n: $v";
	}
	$opts = array(
		'http' => array(
			'timeout' => 10,
			'header' => $headers,
		)
	);
	$context = stream_context_create($opts);
	stream_context_set_default($opts);
        $response_headers = @get_headers($url, 1);
        if (!is_array($response_headers)){
            http_response_code(500);
            exit;
        }
	foreach ($response_headers as $header=>$data) {
		if (is_integer($header))
			header($data);
		elseif (is_array($data)){
                    foreach ($data as $item){
                        header("$header: $item");
                    }
		}
                else
                    header("$header: $data");
	}
	header("Cache-Control: no-cache, no-store, must-revalidate");
    @readfile($url, false, $context);
    exit;
}
if (isset($_GET["test"])){
    $url = getRedirectUrl($_GET['test']);
    if (!$url)
        $url = $_GET['test'];
    $response_headers = get_headers($url, 1);
    header("Content-Type: text/javascript");
    if (isset($response_headers["Content-Length"]))
        echo json_encode(true);
    else
        echo json_encode(false);
    exit;
}
header("Content-Type: text/javascript");
header("Cache-Control: no-cache, no-store, must-revalidate");
$ch = curl_init();
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_URL, "https://nrj.ua/on_air/onair.json");
curl_exec($ch);
if (curl_errno($ch))
    http_response_code(500);
curl_close($ch);
?>
