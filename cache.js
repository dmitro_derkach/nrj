var db;
var downloads = [];
var non_cached = [];
var save_url;
var caching = false;
var cache_session;
(function connectDB(f, error) {
    if (window.indexedDB){
        var request;
        try {
            if (error !== true)
                request = indexedDB.open("vk", {version: 1, storage: "persistent"});
            else
                request = indexedDB.open("vk", 1);
        } catch (error) {
            request = indexedDB.open("vk", 1);
        }
        request.onsuccess = function () {
            // При успешном открытии вызвали коллбэк передав ему объект БД
            if (!request.result.objectStoreNames.contains("vk_cache")) {
                request.result.close();
                indexedDB.deleteDatabase("vk").onsuccess = function () {
                    connectDB(function (d) {
                        db = d;
                        if (window.init_playlist)
                            init_playlist();
                    });
                }
            } else
                f(request.result);
        }
        request.onerror = function () {
            if (!error)
                connectDB(function (d) {
                    db = d;
                    if (window.init_playlist)
                        init_playlist();
                }, true);
        }
        request.onupgradeneeded = function (e) {
            // Если БД еще не существует, то создаем хранилище объектов.
            //e.currentTarget.result.deleteObjectStore("vk_cache");
            e.currentTarget.result.createObjectStore("vk_cache", {keyPath: "id"});
            connectDB(f);
        }
    }
})
        (function (d) {
            db = d;
            if (window.init_playlist)
                init_playlist();
        });
function vkcache(self, silent, success_callback, error_callback, session) {
    var audio = vk_audios[self];
    if (db) {
        if (audio.url) {
            var index = self;
            db.transaction("vk_cache").objectStore("vk_cache").get(audio.id).onsuccess = function (event) {
                if (!event.target.result) {
                    if (downloads.indexOf(index) !== -1) {
                        if (success_callback)
                            success_callback(session);
                        return;
                    }
                    downloads.push(index);
                    var req = new XMLHttpRequest();
                    req.open("GET", "vk.php?url=" + audio.url);
                    req.responseType = 'blob';
                    req.onerror = function (event) {
                        if (downloads.indexOf(index) !== -1)
                            downloads.splice(downloads.indexOf(index), 1);
                        if ($(document.querySelectorAll("#track > div")[index]).find(".download").length) {
                            $(document.querySelectorAll("#track > div")[index]).find(".download").remove();
                        }
                        if (!silent)
                            alert("Network error");
                        if (error_callback)
                            error_callback(session);
                    }
                    req.onprogress = function (event) {
                        if (success_callback && error_callback && (!caching || session !== cache_session)) {
                            if (downloads.indexOf(index) !== -1)
                                downloads.splice(downloads.indexOf(index), 1);
                            if ($(document.querySelectorAll("#track > div")[index]).find(".download").length) {
                                $(document.querySelectorAll("#track > div")[index]).find(".download").remove();
                            }
                            req.abort();
                            return;
                        }
                        if (!$(document.querySelectorAll("#track > div")[index]).find(".download").length && radio_list.value == 5) {
                            $(document.querySelectorAll("#track > div")[index]).append('<div class="download"></div>');
                        }
                        var download = $(document.querySelectorAll("#track > div")[index]).find(".download");
                        if (event.lengthComputable) {
                            download.css("width", 'calc(' + (event.loaded / event.total * 100) + '% - 35px');
                        }
                    }
                    req.onload = function () {
                        if (req.status == 500) {
                            if (!silent)
                                alert("Network error");
                            if (downloads.indexOf(index) !== -1)
                                downloads.splice(downloads.indexOf(index), 1);
                            if (error_callback)
                                error_callback(session);
                            return;
                        }
                        if (downloads.indexOf(index) !== -1)
                            downloads.splice(downloads.indexOf(index), 1);
                        if ($(document.querySelectorAll("#track > div")[index]).find(".download").length) {
                            $(document.querySelectorAll("#track > div")[index]).find(".download").remove();
                        }
                        audio.blob = req.response;
                        var transaction = db.transaction(['vk_cache'], "readwrite");
                        transaction.oncomplete = function () {
                            if (radio_list.value == 5)
                                document.querySelectorAll("#track > div")[index].dataset.cached = true;
                            if (success_callback)
                                success_callback(session);
                        };
                        transaction.onabort = function (e) {
                            if (this.error && !silent)
                                alert(this.error.name);
                            if (error_callback) {
                                if (this.error.name != "QuotaExceededError")
                                    error_callback(session);
                                else {
                                    alert("Cache error");
                                    cancelCache();
                                }
                            }
                        };
                        transaction.onerror = function (e) {
                            if (this.error && !silent)
                                alert(this.error.name);
                            if (error_callback) {
                                if (this.error.name != "QuotaExceededError")
                                    error_callback(session);
                                else {
                                    alert("Cache error");
                                    cancelCache();
                                }
                            }
                        };
                        transaction.objectStore('vk_cache').put(audio);
                    }
                    req.send();
                } else {
                    if (!silent)
                        alert("Already cached");
                    if (success_callback)
                        success_callback(index);
                }
            }
        } else {
            if (!silent)
                alert("Audio is blocked");
        }
    }
}
function vkcache_getAll() {
    if (db) {
        vk_audios = [];
        db.transaction('vk_cache').objectStore('vk_cache').openCursor(null, 'prev').onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                vk_audios.push({
                    id: cursor.key,
                    title: cursor.value.title,
                    artist: cursor.value.artist,
                    url: cursor.value.url,
                    duration: cursor.value.duration,
                    owner_id: cursor.value.owner_id
                });
                cursor.continue();
            } else {
                current_track = 0;
                changeTrack(current_track);
                getPlaylist();
            }
        }
    }
}
function vkcache_delete(self) {
    var audio = vk_audios[self.dataset.self];
    var index = self.dataset.self;
    if (db) {
        db.transaction("vk_cache").objectStore("vk_cache").get(audio.id).onsuccess = function (event) {
            if (event.target.result) {
                var transaction = db.transaction(['vk_cache'], "readwrite");
                transaction.oncomplete = function () {
                    document.querySelectorAll("#track > div")[index].removeAttribute("data-cached");
                };
                transaction.onabort = function (e) {
                    if (this.error)
                        alert(this.error.name);
                };
                transaction.onerror = function (e) {
                    if (this.error)
                        alert(this.error.name);
                };
                transaction.objectStore('vk_cache').delete(audio.id);
            }
        }
    }
}
function getAllNonCached(index, callback) {
    function checkCached(i) {
        if (db) {
            db.transaction("vk_cache").objectStore("vk_cache").get(vk_audios[i].id).onsuccess = function (event) {
                if (!event.target.result) {
                    if (vk_audios[i].url)
                        non_cached.push(vk_audios[i]);
                }
                getAllNonCached(i + 1, callback);
            }
        }
    }
    if (vk_audios && index === undefined) {
        non_cached = [];
        for (var i = 0; i < vk_audios.length; i++) {
            if (vk_audios[i].url) {
                checkCached(i);
                break;
            }
        }
    } else if (vk_audios && index !== undefined) {
        if (vk_audios.length <= index) {
            callback();
            return;
        }
        checkCached(index);
    }
}
function cacheAll() {
    cache_all.innerHTML = 'Cancel downloading';
    caching = true;
    cache_session = getRandomInt(1, 10000);
    getAllNonCached(undefined, function () {
        function success() {
            alert("All songs are cached");
            cancelCache();
        }
        function getIndex(id) {
            for (var i = 0; i < vk_audios.length; i++) {
                if (vk_audios[i].id == id)
                    return i;
            }
            return null;
        }
        (function cache(index, error, session) {
            if (!non_cached.length) {
                alert("All songs are cached");
                cancelCache();
                return;
            }
            cache_all.innerHTML = "(" + (index + 1) + "\\" + non_cached.length + ") " + 'Cancel downloading';
            vkcache(getIndex(non_cached[index].id), true, function (session) {
                if (!caching || session !== cache_session)
                    return;
                index++;
                error = 0;
                if (non_cached.length > index)
                    cache(index, error, session);
                else
                    success();
            }, function (session) {
                if (!caching || session !== cache_session)
                    return;
                error++;
                if (error == 3) {
                    error = 0;
                    index++;
                    if (non_cached.length > index)
                        cache(index, error, session);
                    else
                        success();
                } else
                    cache(index, error, session);
            }, session);
        })(0, 0, cache_session);
    });
}
function clearCache() {
    if (db) {
        db.transaction("vk_cache", "readwrite").objectStore("vk_cache").clear().onsuccess = function (event) {
            for (var i = 0; i < document.querySelectorAll("#track > div").length; i++) {
                document.querySelectorAll("#track > div")[i].removeAttribute("data-cached");
            }
        }
    }
}
function cancelCache() {
    cache_session = 0;
    cache_all.innerHTML = 'Cache all';
    caching = false;
}
function vkdownload(self) {
    function forceDownload(url, filename) {
        var link = window.document.createElement('a');
        link.href = url;
        link.download = filename || 'output.mp3';
        var evt = document.createEvent('MouseEvents');
        evt.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(evt);
    }
    if (save_url) {
        URL.revokeObjectURL(save_url);
        save_url = null;
    }
    if (radio_list.value != 5)
        return;
    var audio = vk_audios[self.dataset.self];
    var index = self.dataset.self;
    db.transaction("vk_cache").objectStore("vk_cache").get(audio.id).onsuccess = function (event) {
        if (event.target.result) {
            save_url = URL.createObjectURL(event.target.result.blob);
            forceDownload(save_url, audio.artist + ' - ' + audio.title + ".mp3");
        } else {
            if (audio.url) {
                var url = window.location.href + '/vk.php?url=' + encodeURIComponent(audio.url) + '&filename=' + encodeURIComponent(audio.artist + ' - ' + audio.title + ".mp3");
                forceDownload(url, audio.artist + ' - ' + audio.title + ".mp3");
            } else
                alert("Audio is blocked");
        }
    }
}