<?php

$opts = array(
	'http' => array(
		'timeout' => 10,
	)
);
stream_context_set_default($opts);

function file_force_download($file, $filename = null) {
	$all_headers = getallheaders();
	$headers = array();

	foreach ($all_headers as $n => $v){
		if (stripos($n, "User-Agent") === false)
			$headers[] = "$n: $v";
	}
	$opts = array(
		'http' => array(
			'timeout' => 10,
			'header' => $headers,
		)
	);
	$context = stream_context_create($opts);
	stream_context_set_default($opts);
    $response_headers = @get_headers($file, 1);
    if (!is_array($response_headers) || !isset($response_headers['Content-Length'])){
        http_response_code(500);
        exit;
    }
    if ($filename) {
        $length = $response_headers['Content-Length'];
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filename)) + '"';
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $length);
    } else {
        foreach ($response_headers as $header=>$data) {
            if (is_integer($header))
                header($data);
            else
                header("$header: $data");
        }
    }
    @readfile($file, false, $context);
    exit;
}

if (isset($_REQUEST["authorize"]) && isset($_REQUEST["login"]) && isset($_REQUEST["password"])){
    $captcha_params = '';
    if (isset($_REQUEST["captcha_sid"]) && isset($_REQUEST["captcha_key"])){
        $captcha_params = "&captcha_sid=" . $_REQUEST["captcha_sid"] . "&captcha_key=" . $_REQUEST["captcha_key"];
    }
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, "https://oauth.vk.com/token?grant_type=password&client_id=3697615&client_secret=AlVXZFMUqyrnABp8ncuU&username=" . $_REQUEST['login'] . "&password=" . $_REQUEST['password'] . "&scope=friends,audio" . $captcha_params);
    $result  = curl_exec($ch);
    if (!curl_errno($ch)){
        echo $result;
    }
    else
        http_response_code(500);
    return;
}
if (isset($_REQUEST['url']) && isset($_REQUEST['filename'])) {
    file_force_download($_REQUEST['url'], $_REQUEST['filename']);
}
if (isset($_REQUEST['url']))
    file_force_download($_REQUEST['url']);
$token = $_GET['token'];
header("Content-Type: text/javascript");
header("Cache-Control: no-cache, no-store, must-revalidate");
$ret = @file_get_contents("https://api.vk.com/method/audio.get?access_token=$token&count=6000&v=5.57");
if ($ret !== false)
    echo $ret;
else
    http_response_code(500);
?>